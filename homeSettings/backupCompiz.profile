[decor]
s0_command = exec /usr/bin/gtk-window-decorator

[core]
s0_active_plugins = core;composite;opengl;imgjpeg;decor;text;place;mousepoll;neg;imgpng;titleinfo;imgsvg;shift;thumbnail;matecompat;move;compiztoolbox;resize;firepaint;regex;cube;td;resizeinfo;showmouse;rotate;obs;cubeaddon;scale;animation;animationsim;expo;animationjc;animationaddon;animationplus;

[resizeinfo]
s0_always_show = true
s0_resizeinfo_font_size = 14

[expo]
s0_expo_edge = 

[showmouse]
s0_initiate = Disabled
s0_initiate_button = <Control><Alt>Button10

[obs]
s0_brightness_increase_button = <Control><Alt>Button4
s0_brightness_decrease_button = <Control><Alt>Button5

[shift]
s0_initiate_key = <Alt><Super>Tab
s0_next_key = <Alt>Tab
s0_prev_key = <Control><Alt>Tab
s0_next_all_key = <Shift><Alt>Tab
s0_prev_all_key = <Shift><Control><Alt>Tab
s0_mode = 1

[cubeaddon]
s0_deformation = 0

[scale]
s0_opacity = 30
s0_button_bindings_toggle = true
s0_initiate_edge = 
s0_initiate_key = Disabled
s0_initiate_button = Button10

[resize]
s0_initiate_button = <Alt>Button10

[animation]
s0_open_effects = animation:Glide 2;animation:Fade;animation:Fade;
s0_close_effects = animation:Glide 2;animation:Fade;animation:Fade;
s0_minimize_effects = animation:Magic Lamp;
s0_unminimize_effects = animation:Magic Lamp;
s0_focus_effects = animation:Dodge;

[firepaint]
s0_initiate_button = <Shift>Button10
s0_clear_key = Disabled
s0_clear_button = <Shift><Control>Button10
s0_bg_brightness = 40.000000
s0_fire_size = 17.000000
s0_fire_slowdown = 0.600000
s0_fire_life = 0.750000
s0_fire_color = #ff7200ff

[cube]
s0_active_opacity = 70.000000


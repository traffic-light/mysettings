export TERM="xterm-256color"

print -P "%F{green}"
cowfortune
print -Pn "%F{yellow}⏰";date; print -Pn "";uptime %f;
for i in {1..21} {21..1} ; do  echo -en "\e[48;5;${i}m \e[0m" ; done ; echo

FILE="/tmp/.lastLogIn"

if [ ! -f "$FILE" ]
then
   # File not found. Create it
   echo "000" > "$FILE"
fi
   
LASTTIME=$(less $FILE)

if [ $LASTTIME -eq $(date +%H) ]
then
    print -P "%F{red}Last update check was within this hour %F{yellow}😀%f"
else
    echo $(date +%H) > "$FILE"
    print -P "%F{red}Checking for updates.. One moment please.. %f"
    sudo pacman -Sy --color=auto &> /dev/null
fi

NUMUPDATES=($(pacman -Qu | wc -l))

if [ $NUMUPDATES -gt 0 ]
then
    print -P "Total packages updates available: %F{red} $NUMUPDATES%f";
else
    print -P "%F{green}There are no package updates!%f"
fi

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/usr/share/oh-my-zsh

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="agnoster"


ZSH_THEME="powerlevel9k/powerlevel9k"
POWERLEVEL9K_HOST_ICON="\uF109 "
#POWERLEVEL9K_HOME_ICON="\uFd24 "
#POWERLEVEL9K_SSH_ICON="\uF489 "
POWERLEVEL9K_USER_ICON="\uF31a"
#POWERLEVEL9K_SUDO_ICON='\uF09C'
POWERLEVEL9K_OS_ICON='\uF09C'
#POWERLEVEL9K_ETC_ICON='\uf423'
#POWERLEVEL9K_WRITABLE_ICON='\uf023'
#POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=$'\ue602'

#Color's for the elements
POWERLEVEL9K_USER_FOREGROUND='white'
POWERLEVEL9K_USER_BACKGROUND='red'

POWERLEVEL9K_HOST_FOREGROUND='black'
POWERLEVEL9K_HOST_BACKGROUND='red'

POWERLEVEL9K_OS_ICON_FOREGROUND='white'
POWERLEVEL9K_OS_ICON_BACKGROUND='blue'

POWERLEVEL9K_ROOT_INDICATOR_ICON_FOREGROUND='yellow'
POWERLEVEL9K_ROOT_INDICATOR_FOREGROUND='red'


POWERLEVEL9K_MODE='nerdfont-complete'
#POWERLEVEL9K_MODE='awesome-fontconfig'
#POWERLEVEL9K_MODE='awesome-patched'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(
    os_icon
    host
    dir
    
    newline
    user
    status
    vcs
    dir_writable
    root_indicator
)
POWERLEVEL9K_SHORTEN_DIR=1
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(root_indicator)
#POWERLEVEL9K_PROMPT_ON_NEWLINE=true


source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh


#------------------------------
# Alias stuff
#------------------------------
# My aliases for frequently used commands:
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -ip'

alias ls='ls --color=auto'
alias lsa='ls -a --color=auto' 
alias lls='ls -lGh --color=auto'
alias llsa='ls -lGha --color=auto'
alias lo='exit'
alias LS='sl'

alias less='less -Si'
alias du='du -h'
alias ssh='ssh -Y'

#installed emacs-nox. But sometimes i type nemacs/ temacs....
#frustrating....
alias temacs='emacs'
alias nemacs='emacs'

alias lns='ln -s'

#aliases for pacman
alias pacin="sudo pacman -S --color=auto"
alias pacrm="sudo pacman -R --color=auto"
alias pacrms="sudo pacman -Rs --color=auto"
alias pacrmsc="sudo pacman -Rsc --color=auto"
alias pacrmdd="sudo pacman -Rdd --color=auto"
alias pacrmn="sudo pacman -Rn --color=auto"
alias pacup="sudo pacman -Sy --color=auto;print -Pn '%B%F{blue}❯❯❯ %f%b total package updates: %F{red}'; pacman -Qu | wc -l; print -Pn '%f'"
alias pacupg="sudo pacman -Syu --color=auto"
alias pacupgf="sudo pacman -Syyu --color=auto"
alias pacsrc="pacman -Ss --color=auto"
alias pacsrcin="sudo pacman -Qs"
alias paclist="sudo pactree"
alias paclistr="sudo pactree -r"
alias pacsrcf="sudo pactree -Fy"

#aliases for yaoert
alias aurin="yaourt"
alias aurup="yaourt -Sy"
alias aurupg="yaourt -Syu"

#graphics
alias nvidia="optirun"

#alliases special for school excercises
alias ops='cd ~/OperatingSystemsLab/exercises'
alias se='cd ~/SoftwareEngineering'
alias roomba='cd ~/SoftwareEngineeringRoomba'

#Grub update
alias mkgrub='sudo emacs /etc/default/grub && sudo grub-mkconfig -o /boot/grub/grub.cfg'


alias su='su -'

#show some cool ascii icons
alias showASCII='echo "✆ 🎮🔒🔓📥📤🏭🍺☢️ ☣️ ☠ ☺☹ ♬ ♲ ⛐ ⛇ ⛈ ⛽⛾ ⛔✇ 💾📷⍰💿📫📬📪💀😮👽💽📺🚯⎚ 🐦✃ 📮🐓🐭🐌🚒"'


# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    zsh-completions
)
autoload -U compinit && compinit
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

#------------------------------
# ShellFuncs
#------------------------------
# -- coloured manuals
man() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    man "$@"
}

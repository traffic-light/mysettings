[decoration]
as_command = emerald --replace
as_mipmap = true

[core]
as_active_plugins = core;decoration;imgjpeg;text;place;stackswitch;mousepoll;grid;titleinfo;png;thumbnail;dbus;widget;group;matecompat;notification;move;resize;svg;firepaint;workarounds;regex;snap;cube;atlantis;resizeinfo;showmouse;rotate;3d;scale;animation;animationsim;expo;animationaddon;animationplus;
s0_outputs = 1920x1080+0+0;1280x720+0+0;
s0_focus_prevention_level = 1

[expo]
as_expo_edge = 

[screensaver]
as_mode = 1
as_after = 0.100000
as_bounce = false
as_cube_rotation_speed = 1.750000
as_cube_zoom = 0.600000

[stackswitch]
as_next_key = <Alt>Tab
as_prev_key = <Alt><Super>Tab
as_next_all_key = <Shift><Alt>Tab
s0_inactive_opacity = 40
s0_tilt = 60
s0_title_font_bold = true
s0_title_font_family = URW Gothic
s0_title_font_size = 13

[snowglobe]
s0_show_snowman = true

[showmouse]
as_initiate = Disabled
as_initiate_button = <Super>Button10

[rotate]
s0_zoom = 0.500000

[anaglyph]
as_screen_toggle_key = <Shift><Control><Primary><Super>s

[thumbnail]
s0_thumb_size = 225
s0_border = 22
s0_thumb_color = #0000009b
s0_window_like = false
s0_font_color = #fff5009b

[cubeaddon]
s0_ground_color1 = #bf3f3f00
s0_ground_color2 = #bf3f3f00
s0_ground_size = 1.000000
s0_intensity = 1.000000
s0_auto_zoom = false
s0_zoom_manual_only = false
s0_mode = 2
s0_deformation = 0
s0_cylinder_manual_only = true
s0_adjust_top = true
s0_adjust_bottom = true
s0_top_color = #ffffff00
s0_bottom_color = #ffffff00
s0_top_images = ;
s0_bottom_images = /home/peter/Pictures/afb/proxy.duckduckgo.com.png;

[move]
as_opacity = 95

[scale]
as_button_bindings_toggle = true
as_initiate_edge = 
as_initiate_key = Disabled
as_initiate_button = Button10
s0_opacity = 50

[cubemodel]
s0_model_filename = ;
s0_model_scale_factor = 0.010000;
s0_model_x_offset = -1.000000;
s0_model_y_offset = -1.000000;
s0_model_z_offset = -1.000000;
s0_model_rotation_plane = 0;
s0_model_rotation_rate = 0.000000;
s0_model_animation = 0;
s0_model_fps = 0;

[animation]
s0_open_effects = animationsim:Expand;animation:Fade;animation:Fade;animationaddon:Burn;
s0_open_durations = 200;150;150;200;
s0_open_matches = (type=Normal | Dialog | ModalDialog) & !(name=mate-screensaver)  & !(name=qtcreator)  & !(class=Mate-panel);(type = menu |PopupMenu | DropdownMenu  | Unknown) & (name=qtcreator);(type=Tooltip | Notification | Utility) & !(name=compiz) & !(title=notify-osd);;
s0_open_options = ;;;;
s0_close_effects = animationsim:Expand;animation:Fade;animation:Fade;
s0_close_matches = (type=Normal | Dialog | ModalDialog | Unknown) & !(name=mate-screensaver) & !(name=qtcreator);(type=Menu | PopupMenu | DropdownMenu) & (name=qtcreator);(type=Tooltip | Notification | Utility) & !(name=compiz) & !(title=notify-osd);
s0_minimize_effects = animationaddon:Airplane;
s0_minimize_durations = 375;
s0_shade_effects = animation:Horizontal Folds;
s0_focus_effects = animation:Dodge;

[firepaint]
as_initiate_button = <Shift>Button10
as_clear_key = Disabled
as_clear_button = <Shift><Control><Primary>Button10
s0_fire_size = 18.000000

[workarounds]
as_legacy_fullscreen = true
as_firefox_menu_fix = true
as_notification_daemon_fix = true
as_qt_fix = true
as_fglrx_xgl_fix = true

[snap]
s0_snap_type = 0;1;
s0_edges_categories = 0;1;

[cube]
s0_color = #ffffffff
s0_scale_image = true
s0_images = /home/peter/Pictures/divpictures/linux.png;
s0_skydome = true
s0_skydome_image = /home/peter/Pictures/divpictures/blue-sky-with-clouds-grassphotorealistic-windows-background-flowers-grass-nature-gfj1movq1-1024x576.jpg
s0_skydome_animated = true
s0_active_opacity = 50.000000
s0_transparent_manual_only = false

[atlantis]
s0_speed_factor = 2.000000
s0_start_crabs_bottom = true
s0_creature_type = 0;1;3;4;9;5;2;7;6;8;
s0_creature_color = #ffffffff;#60a0ffff;#ff7000ff;#ffff00ff;#ff0000ff;#73d216ff;#c4a000ff;#888a85ff;#729fcfff;#555753ff;
s0_creature_number = 5;5;5;5;5;3;1;1;1;1;
s0_creature_size = 4000;3500;3500;3500;4000;5400;6000;8600;9000;20000;
s0_plant_type = 0;1;2;1;
s0_plant_color = #00d010ff;#00d010ff;#8090ff0c;#4ed321d3;
s0_plant_number = 8;8;3;7;
s0_plant_size = 5000;5000;800;100;
s0_ground_color = #edd400ff
s0_water_height = 0.810000
s0_grid_quality = 7
s0_wave_amplitude = 0.080000
s0_wave_frequency = 10.000000
s0_small_wave_frequency = 30.000000
s0_rotate_lighting = true


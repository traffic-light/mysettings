force_color_prompt=yes

#first: define some colors ;-)

COLOR_DEFAULT='\033[39m'
COLOR_LIGHTGRAY='033[37m'
COLOR_GRAY='\033[40m'
COLOR_BLACK='\033[30m'
COLOR_RED='\033[31m'
COLOR_LIGHTRED='\033[91m'
COLOR_GREEN='\033[32m'
COLOR_LIGHTGREEN='\033[92m'
COLOR_YELLOW='\033[33m'
COLOR_LIGHTYELLOW='\033[93m'
COLOR_BLUE='\033[34m'
COLOR_LIGHTBLUE='\033[94m'
COLOR_PURPLE='\033[35m'
COLOR_PINK='\033[95m'
COLOR_CYAN='\033[36m'
COLOR_LIGHTCYAN='\033[96m'

#define all background colors
BGCOLOR_DEFAULT='\033[49m'
BGCOLOR_WHITE='\033[107m'
BGCOLOR_LIGHTGRAY='\033[47m'
BGCOLOR_GRAY='\033[100m'
BGCOLOR_BLACK='\033[40m'
BGCOLOR_RED='\033[41m'
BGCOLOR_LIGHTRED='\033[101m'
BGCOLOR_GREEN='\033[42m'
BGCOLOR_LIGHTGREEN='\033[102m'
BGCOLOR_YELLOW='\033[43m'
BGCOLOR_LIGHTYELLOW='\033[103m'
BGCOLOR_BLUE='\033[44m'
BGCOLOR_LIGHTBLUE='\033[104m'
BGCOLOR_PURPLE='\033[45m'
BGCOLOR_PINK='\033[105m'
BGCOLOR_CYAN='\033[46m'
BGCOLOR_LIGHTCYAN='\033[106m'

#formatting
FORMAT_BOLD='\033[1m'
FORMAT_DIM='\033[2m'
FORMAT_UNDERLINED='\033[4m'
FORMAT_BLINK='\033[5m'
FORMAT_REVERSE='\033[7m'
FORMAT_HIDDEN='\033[8m'

RMFORMAT_BOLD='\033[21m'
RMFORMAT_DIM='\033[22m'
RMFORMAT_UNDERLINED='\033[24m'
RMFORMAT_BLINK='\033[25m'
RMFORMAT_REVERS='\033[27m'
RMFORMAT_HIDDEN='\033[28m'

CLEAR_FORMATS='\033[0m'

echo -e "${COLOR_LIGHTYELLOW}"
date
echo -en "${CLEAR_FORMATS}"

echo -e "${COLOR_LIGHTRED}Checking for updates.. One moment please..${CLEAR_FORMATS}"
pacman -Sy &> /dev/null

declare -i numupdates=($(pacman -Qu | wc -l))

if (($numupdates))
then
    echo -e "Total packages updates available: ${COLOR_LIGHTRED}$numupdates";
else
    echo -e "${COLOR_LIGHTGREEN}There are no package updates!"
fi
echo -e "${CLEAR_FORMATS}"

export PS1="\[${FORMAT_BOLD}\]\[${COLOR_LIGHTRED}\]╭╴\[${COLOR_DEFAULT}\]\[${BGCOLOR_BLUE}\][\[${BGCOLOR_RED}\]\[${FORMAT_BLINK}\]\u\[${RMFORMAT_BLINK}\]\[${BGCOLOR_YELLOW}\]@\[${BGCOLOR_BLUE}\]\h \[${BGCOLOR_GREEN}\]\W\[${BGCOLOR_BLUE}\]]\[${BGCOLOR_YELLOW}\]#\[${BGCOLOR_DEFAULT}\]\n\[${COLOR_LIGHTRED}\]╰╼╸\[${CLEAR_FORMATS}\]" # root user

export EDITOR='emacs'
export VISUAL='emacs'
export PAGER='less'

# History control:
export HISTCONTROL="ignoreboth"   # Ignore repeat commands, commands starting with space
export HISTSIZE=10000             # Make a history file of 10k lines, rather than 500
export HISTFILESIZE=100000        # Make a history file of 100k lines, rather than 500
shopt -s histappend               # Makes bash append to history rather than overwrite

# Shell options:
shopt -s cdspell                  # Correct minor typos in directory names on cd command
shopt -s dirspell                 # Correct minor typos in dir names on tab completion
shopt -s checkjobs                # Do not exit if shell has running/suspended jobs

# Colour in man pages (when using less as a pager - see man termcap):
export LESS_TERMCAP_mb=$'\E[01;34m'  # Blinking -> bold blue
export LESS_TERMCAP_md=$'\E[01;34m'  # Bold (section names, cl options) -> bold blue
export LESS_TERMCAP_me=$'\E[0m'      # End bold/blinking
export LESS_TERMCAP_so=$'\E[01;44m'  # Standout mode - pager -> bold white on blue
export LESS_TERMCAP_se=$'\E[0m'      # End standout
export LESS_TERMCAP_us=$'\E[01;31m'  # Underline - variables -> bold red
export LESS_TERMCAP_ue=$'\E[0m'      # End underline
export GROFF_NO_SGR=1

# My aliases for frequently used commands:
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -ip'
alias ls='ls --color=auto'
alias lsa='ls -a --color=auto' 
alias lls='ls -lGh'
alias llsa='ls -lGha'
alias lo='exit'
alias less='less -Si'
alias du='du -h'
alias ssh='ssh -Y'
alias temacs='emacs'
alias nemacs='emacs'
alias lns='ln -s'

#aliases for pacman
alias pacin="pacman -S"
alias pacrm=" pacman -R"
alias pacrms=" pacman -Rs"
alias pacrmsc=" pacman -Rsc"
alias pacrmdd=" pacman -Rdd"
alias pacrmn=" pacman -Rn"
alias pacup=" pacman -Sy --color=auto;echo -en '${FORMAT_BOLD}${COLOR_LIGHTBLUE}❯❯❯${COLOR_DEFAULT} total package updates: ${COLOR_LIGHTRED}'; pacman -Qu | wc -l; echo -en '${CLEAR_FORMATS}'"
#dangerous (wiki) alias pacupf=" pacman -Syy --color=auto;echo -en '${FORMAT_BOLD}${COLOR_LIGHTBLUE}❯❯❯${COLOR_DEFAULT} total package updates: ${COLOR_LIGHTRED}'; pacman -Qu | wc -l; echo -en '${CLEAR_FORMATS}'"
alias pacupg=" pacman -Syu --color=auto"
# DANGEROUS (wiki): alias pacupgf=" pacman -Syyu --color=auto"
alias pacsrc=" pacman -Ss --color=auto"
alias pacsrcin=" pacman -Qs"
alias paclist=" pactree"
alias paclistr=" pactree -r"
alias pacsrcf=" pactree -Fy"

